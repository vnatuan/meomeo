<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmailsController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\EmailTypeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\UserMoneyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('products',ProductController::class);
Route::resource('email-types',EmailTypeController::class);
Route::resource('invoices',InvoiceController::class);
Route::post('naptien',[UserMoneyController::class, 'addUserAmount']);
Route::get('tongtien/{userId}/loaitiente/{currencyCode}',[UserMoneyController::class, 'getUserAmount']);
Route::get("payments/{hashStr}",[PaymentController::class, 'show']);
Route::get("fpayment-callback",[PaymentController::class, 'callback_fpayment']);
Route::get("payments/invoice/{invoiceId}/user/{userId}",[PaymentController::class, 'getPaymentByInvoiceId']);
Route::get("hoadon/user/{user_id}/type/{type}",[InvoiceController::class, 'getHoaDonByUser']);
Route::post('admin/import-emails', 'App\Http\Controllers\FileController@importEmail');
Route::get('admin/customers-info', 'App\Http\Controllers\AdminController@getCustomersInfo');
Route::get('admin/customers-info/{email}', 'App\Http\Controllers\AdminController@getCustomerInfo');
Route::post('admin/update-money', 'App\Http\Controllers\AdminController@addMoneyCustomer');
// Route::post('nowpayment/invoice', 'App\Http\Controllers\CryptoController@nowpaymentCreateInvoice');
Route::get('emails/get-emails-payment/{hashStr}/type/{type}', array('as' => 'getEmailsPayment', 'uses' => 'App\Http\Controllers\EmailsController@getAndSendEmails'));
Route::get('payments/search/{payment_id}', [PaymentController::class, 'search']);

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/mua-email-tra-truoc', [InvoiceController::class, 'muaEmailTraTruoc']);  
    Route::post('/reset-password', [AuthController::class, 'forgotPassword']);  
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);
    Route::post('/change-pass', [AuthController::class, 'changePassWord']);    
});

