<?php

return [
    'invoice_type' => [
        "mua_hang_tra_sau" => 1,
        "mua_hang_tra_truoc" => 2,
        "nap_tien_vao_tai_khoan" => 3,
    ],
    'is_user' => [
        "la_user_he_thong" => 1,
        "la_nguoi_mua_hang_vang_lai" => 2,
    ],
    'currency' => [
        "usd" => "usd",
    ],
    'pay_currency' => [
        "USDTTRC20" => "USDTTRC20",
    ],
];
