<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emailArray = [
            [
                "name" => "Tuan",
                "email" => "vnatuan1989@gmail.com",
                "password" => Hash::make("111111"),
                "role" => 2,
            ],
            [
                "name" => "Vinh",
                "email" => "vinhpro@gmail.com",
                "password" => Hash::make("111111"),
                "role" => 1,
            ]
        ];
        
        DB::table('users')->insert($emailArray);
    }
}
