<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class EmailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($j = 1 ; $j < 16; $j ++) {
            $emailArray = [];
            for ($i = 0 ; $i < 10000 ; $i ++) {
                $emailArray[] = [
                    "email" => Str::random(40).'@gmail.com',
                    "password" => "password",
                    "recovery_email" => Str::random(40).'@gmail.com',
                    "id_email_type" => $j,
                    "status" => 1
                ];
            }
            DB::table('emails')->insert($emailArray);
        }
    }
}
