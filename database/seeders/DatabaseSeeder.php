<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\EmailTypesSeeder;
use Database\Seeders\ProductsSeeder;
use Database\Seeders\EmailsSeeder;
use Database\Seeders\UserSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmailTypesSeeder::class,
            ProductsSeeder::class,
            EmailsSeeder::class,
            UserSeeder::class,
        ]);
    }
}
