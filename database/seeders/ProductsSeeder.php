<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Products
        DB::table('products')->insert([
            "title" => "Product 1",
            'description' => "Description 1"
        ]);
        DB::table('products')->insert([
            "title" => "Product 2",
            'description' => "Description 2"
        ]);
        DB::table('products')->insert([
            "title" => "Product 3",
            'description' => "Description 3"
        ]);
        DB::table('products')->insert([
            "title" => "Product 4",
            'description' => "Description 4"
        ]);

        //Product Email Type
        DB::table('product_email_type')->insert([
            "id_product" => "1",
            'id_email_type' => "1"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "1",
            'id_email_type' => "14"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "2",
            'id_email_type' => "3"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "2",
            'id_email_type' => "6"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "3",
            'id_email_type' => "2"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "3",
            'id_email_type' => "3"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "4",
            'id_email_type' => "15"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "4",
            'id_email_type' => "11"
        ]);
        DB::table('product_email_type')->insert([
            "id_product" => "4",
            'id_email_type' => "12"
        ]);
    }
}
