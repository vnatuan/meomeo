<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        DB::table('email_types')->insert([
            'description' => "Gmail New Random IP 7-30 days old",
            'ip_create' => "Random",
            'date_create' => "7-30 days",
            'device_create' => "Android",
            'price' => 0.13,
        ]);
        //2
        DB::table('email_types')->insert([
            'description' => "Gmail New VietNam 7-30 days old",
            'ip_create' => "VietNam",
            'date_create' => "7-30 days",
            'device_create' => "Chrome",
            'price' => 0.13,
        ]);
        //3
        DB::table('email_types')->insert([
            'description' => "Gmail New Random IP 7-30 days old",
            'ip_create' => "VietNam",
            'date_create' => "7-30 days",
            'device_create' => "IOS",
            'price' => 0.13,
        ]);
        //4
        DB::table('email_types')->insert([
            'description' => "Gmail New Random ip 30+ days",
            'ip_create' => "Random",
            'date_create' => "7-30 days",
            'device_create' => "IOS",
            'price' => 0.13,
        ]);
        //5
        DB::table('email_types')->insert([
            'description' => "Gmail New Random ip 30+ days old",
            'ip_create' => "Random",
            'date_create' => "30+ days",
            'device_create' => "IOS",
            'price' => 0.15,
        ]);
        //6
        DB::table('email_types')->insert([
            'description' => "Gmail New USA 7-30 days old",
            'ip_create' => "USA",
            'date_create' => "7-15 days",
            'device_create' => "IOS",
            'price' => 0.40,
        ]);
        //7
        DB::table('email_types')->insert([
            'description' => "Gmail New USA 30+ days old",
            'ip_create' => "USA",
            'date_create' => "30+ days",
            'device_create' => "IOS",
            'price' => 0.50,
        ]);
        //8
        DB::table('email_types')->insert([
            'description' => "Gmail New ES 7+ days old",
            'ip_create' => "ES",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //9
        DB::table('email_types')->insert([
            'description' => "Gmail New IT 7+ days old",
            'ip_create' => "IT",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //10
        DB::table('email_types')->insert([
            'description' => "Gmail New AR 7+ days old",
            'ip_create' => "AR",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //11
        DB::table('email_types')->insert([
            'description' => "Gmail New AU 7+ days old",
            'ip_create' => "AU",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //12
        DB::table('email_types')->insert([
            'description' => "Gmail New JP 7+ days old",
            'ip_create' => "JP",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //13
        DB::table('email_types')->insert([
            'description' => "Gmail New CA 7+ days old",
            'ip_create' => "CA",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //14
        DB::table('email_types')->insert([
            'description' => "Gmail New DE 7+ days old",
            'ip_create' => "DE",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
        //15
        DB::table('email_types')->insert([
            'description' => "Gmail New OM 7+ days old",
            'ip_create' => "OM",
            'date_create' => "7+ days",
            'device_create' => "IOS",
            'price' => 0.30,
        ]);
    }
}
