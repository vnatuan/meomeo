<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string("invoice_url")->nullable();
            $table->string("pay_currency")->nullable();
            $table->string("price_currency")->nullable();
            $table->double("price_amount")->nullable();
            $table->string("order_description")->nullable();
            $table->string("order_id")->nullable();
            $table->string("iid")->nullable();
            $table->string("token_id")->nullable();
            $table->bigInteger("id_customer")->nullable();
            $table->integer("is_user")->nullable();
            $table->integer("type")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
