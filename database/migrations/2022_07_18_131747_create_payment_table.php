<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string("payment_id")->nullable();
            $table->string("payment_status")->nullable();
            $table->string("pay_address")->nullable();
            $table->double("price_amount")->nullable();
            $table->string("price_currency")->nullable();
            $table->double("pay_amount")->nullable();
            $table->double("amount_received")->nullable();
            $table->string("pay_currency")->nullable();
            $table->tinyInteger("is_assigned_emails")->nullable()->default(0);
            $table->bigInteger("purchase_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
