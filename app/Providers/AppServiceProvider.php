<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Products
        $this->app->singleton(
            \App\Repositories\Product\ProductRepositoryInterface::class,
            \App\Repositories\Product\ProductRepository::class
        );
        //Email Types
        $this->app->singleton(
            \App\Repositories\EmailType\EmailTypeRepositoryInterface::class,
            \App\Repositories\EmailType\EmailTypeRepository::class
        );
        //Invoices
        $this->app->singleton(
            \App\Repositories\Invoice\InvoiceRepositoryInterface::class,
            \App\Repositories\Invoice\InvoiceRepository::class
        );
        //Customer
        $this->app->singleton(
            \App\Repositories\Customer\CustomerRepositoryInterface::class,
            \App\Repositories\Customer\CustomerRepository::class
        );
        //Payments
        $this->app->singleton(
            \App\Repositories\Payment\PaymentRepositoryInterface::class,
            \App\Repositories\Payment\PaymentRepository::class
        );
        //Email
        $this->app->singleton(
            \App\Repositories\Email\EmailRepositoryInterface::class,
            \App\Repositories\Email\EmailRepository::class
        );
        //User Money
        $this->app->singleton(
            \App\Repositories\UserMoney\UserMoneyRepositoryInterface::class,
            \App\Repositories\UserMoney\UserMoneyRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
