<?php
namespace App\Repositories\Customer;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    public $customerModel;
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Customer::class;
    }
    public function getCustomersInfo()
    {
        $customers = DB::table("users")
        ->join("user_money" , "users.id" , "=", "user_money.user_id")
        ->select("users.id", "users.name", "users.email", "user_money.total_amount", "user_money.currency_code")
        ->get();
        return $customers;
    }
    
    public function getCustomerInfo($email) {
        $customer = DB::table("users")
        ->join("user_money" , "users.id" , "=", "user_money.user_id")
        ->select("users.id", "users.name", "users.email", "user_money.total_amount", "user_money.currency_code")
        ->where("users.email", '=', $email)
        ->first();
        return $customer;
    }
}