<?php
namespace App\Repositories\Email;

use App\Repositories\BaseRepository;
use App\Repositories\Customer\CustomerRepositoryInterface;
use App\Repositories\Invoice\InvoiceRepository;
use App\Models\User;

class EmailRepository extends BaseRepository implements EmailRepositoryInterface
{
    public $customerRepo;
    public $invoiceRepo;
    public $userModel;

    public function __construct(
        CustomerRepositoryInterface $customerRepo,
        InvoiceRepository $invoiceRepo,
        User $userModel
    )
    {
        $this->setModel();
        $this->customerRepo = $customerRepo;
        $this->invoiceRepo = $invoiceRepo;
        $this->userModel = $userModel;
    }

    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Email::class;
    }

    public function getEmails($decodeStr, $type)
    {
        $arr = explode("|", $decodeStr);
        $customer = [];
        if(!empty($arr)){
            $hoadon = $this->invoiceRepo->layHoaDonByPaymentId($arr[1]);
            if ($hoadon->is_user == 1) {
                $customer = $this->userModel->where(["id" => $arr[0]])->first();
            } else {
                $customer = $this->customerRepo->find($arr[0]);
            }
            $emails = $this->where(["id_payment" => $arr[1]])->get();
        }
        switch ($type) {
            case 'txt':
                $type = '.csv';
                break;
            default:
                $type = '.xlsx';
                break;
        }
        return [
            'customer' => $customer,
            'fileName' => $customer->email . '_' . $arr[1] . $type, 
            'fileNameTxt' => $customer->email . '_' . $arr[1] . '.txt', 
            'emails' => $emails
        ];
    }
}