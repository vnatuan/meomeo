<?php
namespace App\Repositories\Product;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Product::class;
    }

    public function getProduct($id)
    {
        $product = $this->model->find($id);
        $product->email_types = $product->emailTypes;
        
        return $product;
    }
}