<?php
namespace App\Repositories\EmailType;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class EmailTypeRepository extends BaseRepository implements EmailTypeRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\EmailType::class;
    }

    public function checkInStock($idEmailType, $quantity)
    {
        $check = DB::table("emails")
            ->selectRaw("count(*) as count")
            ->where("status" , "=", 1)
            ->where("id_email_type" , "=", $idEmailType)
            ->havingRaw("count(*) >= " . $quantity)
            ->first();
        return !empty($check) ? true : false;
    }
}