<?php
namespace App\Repositories\Invoice;

use App\Repositories\BaseRepository;
use App\Helper\CryptoHelper;
use App\Helper\Helper;
use App\Repositories\Payment\PaymentRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class InvoiceRepository extends BaseRepository implements InvoiceRepositoryInterface
{
    public $cryptoHelper;
    public $paymentRepo;
    public $helper;
    public $nowpaymentSuccessUrl;

    public function __construct(
        CryptoHelper $cryptoHelper,
        Helper $helper,
        PaymentRepository $paymentRepo
    )
    {
        $this->setModel();
        $this->cryptoHelper = $cryptoHelper;
        $this->paymentRepo = $paymentRepo;
        $this->helper = $helper;
        $this->nowpaymentSuccessUrl = env('NOWPAYMENT_SUCCESS_URL');
    }

    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Invoice::class;
    }

    public function createInvoiceTraSau($data)
    {
        $priceAmount = floatval($data['quantity']) * floatval($data['price']);
        $priceCurrency = Config::get("app_private.currency.usd");
        $payCurrency = $data['payment_type'];
        $orderDescription = $data['customer_email'] . "_ mua hang tra sau _" . time();
        $orderId = time();
        //CREATE INVOICE
        $resultInvoice = $this->cryptoHelper->nowpayemntCreateInvoiceSandBox(
            $priceAmount, 
            $priceCurrency, 
            $payCurrency, 
            $orderId, 
            $orderDescription,
            Config::get("app_private.invoice_type.mua_hang_tra_sau")
        );
       
        if (!empty($resultInvoice)) {
            $invoiceData = [
                "invoice_url" => $resultInvoice['invoice_url'],
                "pay_currency" => $resultInvoice['pay_currency'],
                "price_currency" => $resultInvoice['price_currency'],
                "price_amount" => $resultInvoice['price_amount'],
                "order_description" => $orderDescription,
                "order_id" => $resultInvoice['order_id'],
                "iid" => $resultInvoice['id'],
                "token_id" => $resultInvoice['token_id'],
                "id_customer" => $data['id_customer'],
                "id_email_type" => $data['id_email_type'],
                "payment_type" => $resultInvoice['pay_currency'],
                "quantity" => $data['quantity'],
                "is_user" => $data['is_user'],
                "type" => $data['type'],
            ];
            $invoice = $this->create($invoiceData);
            
            $resultPayment = $this->paymentRepo->createPaymentTraSau(
               [
                "iid" => $invoice['iid'],
                "pay_currency" => $invoice['pay_currency'],
                "customer_email" => $data['customer_email'],
                "id_invoice" => $invoice['id'],
               ]
            );
            $resultInvoice['invoice_url'] = $resultInvoice['invoice_url'] . "&paymentId=" . $resultPayment['payment_id'];
            return $resultInvoice;
        }
        return false;
    }

    public function createInvoiceTraTruoc($data)
    {
        $priceAmount = floatval($data['quantity']) * floatval($data['price']);
        $priceCurrency = Config::get("app_private.currency.usd");
        $payCurrency = $data['payment_type'];
        $orderDescription = $data['customer_email'] . "_ mua hang tra truoc _" . time();
        $orderId = time();
        
        $invoiceData = [
            "pay_currency" => $payCurrency,
            "price_currency" => $priceCurrency,
            "price_amount" => $priceAmount,
            "order_description" => $orderDescription,
            "order_id" => $orderId,
            "id_customer" => $data['id_customer'],
            "id_email_type" => $data['id_email_type'],
            "payment_type" => $payCurrency,
            "quantity" => $data['quantity'],
            "is_user" => $data['is_user'],
            "type" => $data['type'],
        ];
        
        $invoice = $this->create($invoiceData);
        
        $resultPayment = $this->paymentRepo->createPaymentTraTruoc(
            [
            "price_amount" => $priceAmount,
            "price_currency" => $priceCurrency,
            "price_amount" => $priceAmount,
            "pay_currency" => $invoice['pay_currency'],
            "customer_email" => $data['customer_email'],
            "id_invoice" => $invoice['id'],
            ]
        );
        $invoice->success_url = $this->nowpaymentSuccessUrl . $this->helper->enCryptDeCryptMethod('encrypt', $orderId . "|||" . $data['type']);
        $invoice->invoice_url = "/";
        return $invoice;
    }


    public function createInvoiceForAddAmount($data)
    {
        $priceAmount = $data['amount'];
        $priceCurrency = Config::get("app_private.currency.usd");
        $payCurrency = Config::get("app_private.pay_currency.USDTTRC20");
        $orderDescription = $data['user_email'] . "_ nap tien vao tai khoan _" . time();
        $orderId = time();
        //CREATE INVOICE
        $resultInvoice = $this->cryptoHelper->nowpayemntCreateInvoiceSandBox(
            $priceAmount, 
            $priceCurrency, 
            $payCurrency, 
            $orderId, 
            $orderDescription,
            
            Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan")
        );
        if (!empty($resultInvoice)) {
            $invoiceData = [
                "invoice_url" => $resultInvoice['invoice_url'],
                "pay_currency" => $resultInvoice['pay_currency'],
                "price_currency" => $resultInvoice['price_currency'],
                "price_amount" => $resultInvoice['price_amount'],
                "order_description" => $orderDescription,
                "order_id" => $resultInvoice['order_id'],
                "iid" => $resultInvoice['id'],
                "token_id" => $resultInvoice['token_id'],
                "id_customer" => $data['id_customer'],
                "id_email_type" => null,
                "payment_type" => $resultInvoice['pay_currency'],
                "quantity" => "1",
                "is_user" => $data['is_user'],
                "type" => $data['type'],
            ];
            $invoice = $this->create($invoiceData);
            
            $resultPayment = $this->paymentRepo->createPaymentTraSau(
               [
                "iid" => $invoice['iid'],
                "pay_currency" => $invoice['pay_currency'],
                "customer_email" => $data['user_email'],
                "id_invoice" => $invoice['id'],
               ]
            );
            $resultInvoice['invoice_url'] = $resultInvoice['invoice_url'] . "&paymentId=" . $resultPayment['payment_id'];
            return $resultInvoice;
        }
        return false;
    }

    public function createInvoiceForAddAmount_Fpayment($data)
    {
        $priceAmount = $data['amount'];
        $priceCurrency = Config::get("app_private.currency.usd");
        $payCurrency = Config::get("app_private.pay_currency.USDTTRC20");
        $orderDescription = $data['user_email'] . "_ nap tien vao tai khoan _" . time();
        $orderId = md5("FPAYMENT_" . time());
        $name = "Nạp Tiền Vào Tài Khoản";
        
        //CREATE INVOICE
        $resultInvoice = $this->cryptoHelper->fpaymentCreateInvoice(
            $name, 
            $orderDescription, 
            $priceAmount, 
            $orderId,
            Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan")
        );
       
        if (!empty($resultInvoice)) {
            $invoiceData = [
                "invoice_url" => $resultInvoice['data']['url_payment'],
                "pay_currency" => $payCurrency,
                "price_currency" => $priceCurrency,
                "price_amount" => $resultInvoice['data']['amount'],
                "order_description" => $orderDescription,
                "order_id" => $resultInvoice['data']['request_id'],
                "iid" => $resultInvoice['data']['trans_id'],
                "token_id" => "",
                "id_customer" => $data['id_customer'],
                "id_email_type" => null,
                "payment_type" => $payCurrency,
                "quantity" => "1",
                "is_user" => $data['is_user'],
                "type" => $data['type'],
            ];
            $invoice = $this->create($invoiceData);
            
            $this->paymentRepo->createPaymentTraSau_Fpayment($invoice);

            $resultInvoice['invoice_url'] = $resultInvoice['data']['url_payment'];
            return $resultInvoice;
        }
        return false;
    }

    public function createInvoiceTraSau_Fpayment($data)
    {
        $priceAmount = floatval($data['quantity']) * floatval($data['price']);
        $priceCurrency = Config::get("app_private.currency.usd");
        $payCurrency = Config::get("app_private.pay_currency.USDTTRC20");
        $orderDescription = $data['customer_email'] . "_ mua hang tra sau _" . time();
        $orderId = md5("FPAYMENT_" . time());
        $name = "Nạp Tiền Vào Tài Khoản";
        
        //CREATE INVOICE
        $resultInvoice = $this->cryptoHelper->fpaymentCreateInvoice(
            $name, 
            $orderDescription, 
            $priceAmount, 
            $orderId,
            Config::get("app_private.invoice_type.mua_hang_tra_sau")
        );
       
        if (!empty($resultInvoice)) {
            $invoiceData = [
                "invoice_url" => $resultInvoice['data']['url_payment'],
                "pay_currency" => $payCurrency,
                "price_currency" => $priceCurrency,
                "price_amount" => $resultInvoice['data']['amount'],
                "order_description" => $orderDescription,
                "order_id" => $resultInvoice['data']['request_id'],
                "iid" => $resultInvoice['data']['trans_id'],
                "token_id" => "",
                "id_customer" => $data['id_customer'],
                "id_email_type" => $data['id_email_type'],
                "payment_type" => $payCurrency,
                "quantity" => $data['quantity'],
                "is_user" => $data['is_user'],
                "type" => $data['type'],
            ];
            $invoice = $this->create($invoiceData);
            
            $this->paymentRepo->createPaymentTraSau_Fpayment($invoice);

            $resultInvoice['invoice_url'] = $resultInvoice['data']['url_payment'];
            
            return $resultInvoice;
        }
        return false;
    }

    public function layDanhSachHoaDonByUserId($userId, $type) {
        $searchType = $type;
        $donhang = DB::table("invoices")
        ->join("payments" , "payments.id_invoice" , "=", "invoices.id")
        ->leftJoin("email_types" , "email_types.id" , "=", "invoices.id_email_type")
        ->select("email_types.description", "invoices.quantity", "invoices.price_amount", "invoices.price_currency","payments.payment_status", "invoices.id as invoice_id", "invoices.order_id", "invoices.created_at")
        ->where("invoices.id_customer", "=", $userId)
        ->where("invoices.type", "=", $searchType)
        ->where("payments.payment_status", "=", "finished")
        ->get();
        return $donhang;
    }

    public function layHoaDonByPaymentId($paymentId) {
        $donhang = DB::table("invoices")
        ->join("payments" , "payments.id_invoice" , "=", "invoices.id")
        ->select("invoices.quantity","invoices.is_user", "invoices.price_amount", "invoices.price_currency","payments.payment_status", "invoices.id as invoice_id", "invoices.order_id", "invoices.created_at")
        ->where("payments.id", "=", $paymentId)
        ->first();
        return $donhang;
    }
}