<?php
namespace App\Repositories\Payment;

use App\Repositories\BaseRepository;
use App\Helper\CryptoHelper;
use Illuminate\Support\Facades\DB;

class PaymentRepository extends BaseRepository implements PaymentRepositoryInterface
{
    public $cryptoHelper;

    public function __construct(CryptoHelper $cryptoHelper)
    {
        $this->setModel();
        $this->cryptoHelper = $cryptoHelper;
    }

    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\Payment::class;
    }

    public function createPaymentTraSau($data)
    {
        $resultPayment = $this->cryptoHelper->nowpayemntCreatePaymentForInvoiceSandBox(
            $data['iid'],
            $data['pay_currency'],
            $data['customer_email']
        );
        
        if (!empty($resultPayment)) {
            $paymentData = [
                "payment_id" => $resultPayment['payment_id'],
                "payment_status" => $resultPayment['payment_status'],
                "pay_address" => $resultPayment['pay_address'],
                "price_amount" => $resultPayment['price_amount'],
                "price_currency" => $resultPayment['price_currency'],
                "pay_amount" => $resultPayment['pay_amount'],
                "amount_received" => $resultPayment['amount_received'],
                "pay_currency" => $resultPayment['pay_currency'],
                "purchase_id" => $resultPayment['purchase_id'],
                "id_invoice" => $data['id_invoice'],
            ];
            $payment = $this->create($paymentData);
            return $payment;
        }
        return false;
    }

    public function createPaymentTraSau_Fpayment($data)
    {
        if (!empty($data)) {
            $paymentData = [
                "payment_id" => $data['order_id'],
                "payment_status" => 'waiting',
                "pay_address" => "",
                "price_amount" => $data['price_amount'],
                "price_currency" => $data['price_currency'],
                "pay_amount" => $data['price_amount'],
                "amount_received" => $data['price_amount'],
                "pay_currency" => $data['price_currency'],
                "purchase_id" => $data['purchase_id'],
                "id_invoice" => $data['id'],
            ];
            
            $payment = $this->create($paymentData);
            return $payment;
        }
        return false;
    }

    public function createPaymentTraTruoc($data)
    {   
        $paymentData = [
            "payment_status" => "finished",
            "price_amount" => $data['price_amount'],
            "price_currency" => $data['price_currency'],
            "pay_amount" => $data['price_amount'],
            "amount_received" => $data['price_amount'],
            "pay_currency" => $data['pay_currency'],
            "id_invoice" => $data['id_invoice'],
        ];
        $payment = $this->create($paymentData);
        return $payment;
    }

    public function getPayment($orderId)
    {        
        $payment = DB::table("payments")
        ->join("invoices" , "payments.id_invoice" , "=", "invoices.id")
        ->select("payments.*", "invoices.id_customer", "invoices.id_email_type", "invoices.type", "invoices.quantity")
        ->where("invoices.order_id", "=", $orderId)
        ->first();
        return $payment;
    }

    public function getPaymentByInvoiceId($invoiceId, $userId)
    {        
        $payment = DB::table("payments")
        ->join("invoices" , "payments.id_invoice" , "=", "invoices.id")
        ->select("payments.*", "invoices.id_customer", "invoices.id_email_type", "invoices.quantity")
        ->where("invoices.id_customer", "=", $userId)
        ->where("invoices.id", "=", $invoiceId)
        ->first();
        return $payment;
    }

    public function assignEmails($paymentId, $emailType, $quantity)
    {
        $emailsIdArr = DB::table("emails")
            ->select("id")
            ->where("status", "=", "1")
            ->where("id_email_type", "=", $emailType)
            ->limit($quantity)
            ->get()->pluck("id")
            ->toArray();
        if (!empty($emailsIdArr)) {
            $emailsId = implode(",", $emailsIdArr);
            DB::statement("update emails set status = 2 , id_payment = '" . $paymentId ."' where id in (" . $emailsId . ");");
        }
    }

    public function getStatusInLive($paymentId)
    {
        $paymentInLive = $this->cryptoHelper->nowpayemntCheckPaymentStatusSandBox($paymentId);
        return !empty($paymentInLive) ? $paymentInLive['payment_status'] : null;
    }

    public function accessCallBack($data)
    {    
        DB::statement("update payments set payment_status = '" . $data['status'] . "' where payment_id = '" . $data['request_id'] . "';");    
    }
}