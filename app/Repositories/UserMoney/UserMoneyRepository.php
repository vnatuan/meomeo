<?php
namespace App\Repositories\UserMoney;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class UserMoneyRepository extends BaseRepository implements UserMoneyRepositoryInterface
{
   

    public function __construct(
    )
    {
        $this->setModel();
    }

    //lấy model tương ứng
    public function getModel()
    {
        return \App\Models\UserMoney::class;
    }
}