<?php

namespace App\Http\Controllers;

use App\Services\AdminServices;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    private $adminServices;


    public function __construct(AdminServices $adminServices)
    {
        $this->adminServices = $adminServices;
    }

    public function getCustomersInfo() {
        try{
            $result = $this->adminServices->getCustomersInfo();
            return response()->json([
                "data" =>  $result,
                "status" => 200
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function getCustomerInfo($email) {
        try{
            $result = $this->adminServices->getCustomerInfo($email);
            return response()->json([
                "customer" =>  $result
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }

    public function addMoneyCustomer(Request $request)
    {
        try{  
            $request->validate([
                'user_id'=>'required',
                'amount'=>'required|numeric'
            ]);
            $data = $request->all();
            $result = $this->adminServices->addMoneyCustomer($data['user_id'], $data['amount']);
            return response()->json([
                "data" =>  $result,
                "status" => 200
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ],500);
        }
    }
}
