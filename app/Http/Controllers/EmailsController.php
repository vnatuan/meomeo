<?php

namespace App\Http\Controllers;

use App\Models\Email;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\Repositories\Email\EmailRepositoryInterface;
use App\Helper\Helper;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Excel as BaseExcel;
use App\Exports\EmailsExport;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EmailsController extends Controller
{
     /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $emailRepo;
    protected $helperClass;

    public function __construct(
        EmailRepositoryInterface $emailRepo,
        Helper $helperClass
    )
    {
        $this->emailRepo = $emailRepo;
        $this->helperClass = $helperClass;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Email::select('id','email', 'password','recovery_email', 'status')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'=>'required',
            'password'=>'required',
            'recovery_email'=>'required'
        ]);

        $requestData = $request->all();


        try{
            $requestData['password'] = Hash::make($requestData['password']);
            
            Email::create($requestData);

            return response()->json([
                'message'=>'Email Created Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while creating a Email!!'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function show(Email $Email)
    {
        return response()->json([
            'Email'=>$Email
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $Email)
    {
        $request->validate([
            'email'=>'required',
            'recovery_email'=>'required'
        ]);

        $requestData = $request->all();

        try{
            $requestData['password'] = Hash::make($requestData['password']);

            $Email->fill($requestData)->update();

            return response()->json([
                'message'=>'Email Updated Successfully!!'
            ]);

        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while updating a Email!!'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Email  $Email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $Email)
    {
        try {
            $Email->delete();

            return response()->json([
                'message'=>'Email Deleted Successfully!!'
            ]);   
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while deleting a Email!!'
            ]);
        }
    }

    public function getAndSendEmails($idPaymentHash, $type)
    {
        //DECODE ORDER ID
        $decodeStr = $this->helperClass->enCryptDeCryptMethod ('decrypt', $idPaymentHash);
        $data = $this->emailRepo->getEmails($decodeStr, $type);
        if ($type == "txt") {
            
            Excel::store(new EmailsExport($data['emails']), $data['fileName']);
            Storage::delete($data['fileNameTxt']);
            Storage::move($data['fileName'], $data['fileNameTxt']);
            $location = storage_path("app/" . $data['fileNameTxt']);
            if (!empty($data['customer'])) {
                Mail::send(['html'=>'sendmail'], $data, function($message) use ($data, $location){
                    $message->to($data['customer']->email)->subject('This is test email');
                    //$message->to('vnatuan1989@gmail.com')->subject('This is test email');
            
                    $message->from('vnatuan1989@gmail.com', "tuan");
            
                    $message->attach($location);
            
                });
            }
            //return download file
            return Storage::download($data['fileNameTxt']);
        } else {
            $attachment = Excel::raw(
                new EmailsExport($data['emails']), 
                BaseExcel::XLSX
            );
            if (!empty($data['customer'])) {
                Mail::send(['html'=>'sendmail'], $data, function($message) use ($data, $attachment){
                    $message->to($data['customer']->email)->subject('meomeo.io email list');
                    //$message->to('vnatuan1989@gmail.com')->subject('This is test email');
            
                    $message->from('vnatuan1989@gmail.com', "tuan");
            
                    $message->attachData($attachment, $data['fileName']);
            
                });
            }
            return Excel::download(new EmailsExport($data['emails']), $data['fileName']);
        }
    }

    public function getAndSendEmailsV2($idPaymentHash, $type)
    {
        //DECODE ORDER ID
        $decodeStr = $this->helperClass->enCryptDeCryptMethod ('decrypt', $idPaymentHash);
        $data = $this->emailRepo->getEmails($decodeStr, $type);
        if ($type == "txt") {
            
            Excel::store(new EmailsExport($data['emails']), $data['fileName']);
            Storage::delete($data['fileNameTxt']);
            Storage::move($data['fileName'], $data['fileNameTxt']);
            $location = storage_path("app/" . $data['fileNameTxt']);
            if (!empty($data['customer'])) {
                Mail::send(['html'=>'sendmail'], $data, function($message) use ($data, $location){
                    $message->to($data['customer']->email)->subject('meomeo.io email list');
                    //$message->to('vnatuan1989@gmail.com')->subject('This is test email');
            
                    $message->from('vnatuan1989@gmail.com', "Meomeo.io");
            
                    $message->attach($location);
            
                });
            }
            //return download file
            return Storage::download($data['fileNameTxt']);
        } else {
            $attachment = Excel::raw(
                new EmailsExport($data['emails']), 
                BaseExcel::XLSX
            );
            if (!empty($data['customer'])) {
                Mail::send(['html'=>'sendmail'], $data, function($message) use ($data, $attachment){
                    $message->to($data['customer']->email)->subject('This is test email');
                    //$message->to('vnatuan1989@gmail.com')->subject('This is test email');
            
                    $message->from('vnatuan1989@gmail.com', "Meomeo.io");
            
                    $message->attachData($attachment, $data['fileName']);
            
                });
            }
            return Excel::download(new EmailsExport($data['emails']), $data['fileName']);
        }
    }
}
