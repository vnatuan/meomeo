<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostInvoiceAddAmountRequest;
use App\Repositories\Invoice\InvoiceRepositoryInterface;
use App\Models\User as UserModel;
use App\Models\UserMoney as UserMoneyModel;
use App\Helper\Helper;
use Illuminate\Support\Facades\Config;

class UserMoneyController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $invoiceRepo;
    protected $customerRepo;
    protected $emailTypeRepo;
    protected $userModel;
    protected $userMoneyModel;
    public $helperClass;
    public $nowpaymentSuccessUrl;

    public function __construct(
        InvoiceRepositoryInterface $invoiceRepo,
        UserModel $userModel,
        Helper $helperClass,
        UserMoneyModel $userMoneyModel
    ) {
        $this->invoiceRepo = $invoiceRepo;
        $this->userModel = $userModel;
        $this->userMoneyModel = $userMoneyModel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = $this->invoiceRepo->getAll();
        return $invoices;
    }

    // public function addUserAmount(PostInvoiceAddAmountRequest $request) {
    //     $data = $request->all();

    //     try {
    //         $user = null;
            
    //         //For USer
    //         //1 - check
    //         $user = $this->userModel->where(['email' => $data['user_email']])->first();

    //         //2 - register if don't exist
    //         if (empty($user)) {
    //             return response()->json([
    //                 'message'=> "User is not exist!"
    //             ], 500);
    //         }
    //         $data['id_customer'] = $user->id;
    //         $data['is_user'] = Config::get("app_private.is_user.la_user_he_thong");
    //         $data['type'] = Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan");
    //         //For Invoice
    //         $invoice = $this->invoiceRepo->createInvoiceForAddAmount($data);
    //         return response()->json([
    //             $invoice
    //         ]);
    //     } catch(\Exception $e) {
    //         return response()->json([
    //             'message'=> $e->getMessage()
    //         ], 500);
    //     }
    // }

    public function addUserAmount(PostInvoiceAddAmountRequest $request) {
        $data = $request->all();

        try {
            $user = null;
            
            //For USer
            //1 - check
            $user = $this->userModel->where(['email' => $data['user_email']])->first();

            //2 - register if don't exist
            if (empty($user)) {
                return response()->json([
                    'message'=> "User is not exist!"
                ], 500);
            }
            $data['id_customer'] = $user->id;
            $data['is_user'] = Config::get("app_private.is_user.la_user_he_thong");
            $data['type'] = Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan");
            //For Invoice
            $invoice = $this->invoiceRepo->createInvoiceForAddAmount_Fpayment($data);
            return response()->json([
                $invoice
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message'=> $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function getUserAmount($userId, $currencyCode)
    {
        $userAmount = $this->userMoneyModel->where(["user_id" => $userId, "currency_code" => $currencyCode])->first();

        return response()->json([
            'userAmount'=>$userAmount
        ]);
    }
}