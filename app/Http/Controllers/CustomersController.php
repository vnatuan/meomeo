<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Customer;
use App\Helper\CryptoHelper;

class CustomersController extends Controller
{
    public $cryptoHelper;

    public function __construct(CryptoHelper $cryptoHelper)
    {
        $this->cryptoHelper = $cryptoHelper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::select('id', 'email', 'crypto_wallet_address', 'crypto_wallet_privatekey', 'crypto_wallet_hex_address', 'status')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'=>'required'
        ]);

        $requestData = $request->all();
        
        try{  
            $trc20Wallet = $this->cryptoHelper->trc20NewAddress();
            $requestData['crypto_wallet_address'] = $trc20Wallet['address'];
            $requestData['crypto_wallet_privatekey'] = $trc20Wallet['privatekey'];
            $requestData['crypto_wallet_hex_address'] = $trc20Wallet['hexaddress'];
            Customer::create($requestData);

            return response()->json([
                'message'=>'Customer Created Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while creating a Customer!!'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $Customer)
    {
        return response()->json([
            'Customer'=>$Customer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $Customer)
    {
        $request->validate([
            'email'=>'required'
        ]);

        $requestData = $request->all();

        try{

            $Customer->fill($requestData)->update();

            return response()->json([
                'message'=>'Customer Updated Successfully!!'
            ]);

        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while updating a Customer!!'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $Customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $Customer)
    {
        try {
            $Customer->delete();

            return response()->json([
                'message'=>'Customer Deleted Successfully!!'
            ]);   
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while deleting a Customer!!'
            ]);
        }
    }
}
