<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Repositories\Payment\PaymentRepositoryInterface;
use App\Repositories\Invoice\InvoiceRepositoryInterface;
use App\Repositories\UserMoney\UserMoneyRepositoryInterface;
use App\Helper\Helper;
use Illuminate\Support\Facades\URL;
use App\Models\UserMoney as UserMoneyModel;
use App\Models\User as UserModel;
use Illuminate\Support\Facades\Config;

class PaymentController extends Controller
{

    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $paymentRepo;
    protected $invoiceRepo;
    protected $helperClass;
    protected $userMoneyModel;
    protected $userModel;
    protected $userMoneyRepo;

    public function __construct(
        PaymentRepositoryInterface $paymentRepo,
        InvoiceRepositoryInterface $invoiceRepo,
        Helper $helperClass,
        UserMoneyModel $userMoney,
        UserModel $user,
        UserMoneyRepositoryInterface $userMoneyRepo,
    )
    {
        $this->paymentRepo = $paymentRepo;
        $this->invoiceRepo = $invoiceRepo;
        $this->helperClass = $helperClass;
        $this->userMoneyModel = $userMoney;
        $this->userModel = $user;
        $this->userMoneyRepo = $userMoneyRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = $this->paymentRepo->getAll();
        return $payments;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function show($hashStr)
    {
       
        //DECODE ORDER ID
        $hashInformation = $this->helperClass->enCryptDeCryptMethod ('decrypt', $hashStr);
        

        $arr = explode("|||", $hashInformation);
        $orderId = ($arr[0] != null) ? $arr[0] : "";
        $type = ($arr[1] != null) ? $arr[1] : "";
        if ($type == Config::get("app_private.invoice_type.mua_hang_tra_sau")) {
            //GET PAYMENT IN DATABASE
            $payment = $this->paymentRepo->getPayment($orderId);
                    
            if(empty($payment)) {
                return response()->json([
                    'message'=>'Nothing found!!'
                ],500);
            }

            //GET PAYMENT REAL STATUS
            //$status = $this->paymentRepo->getStatusInLive($payment->payment_id);

            //UPDATE PAYMENT IF CHANGED AND ASSIGN EMAILS FOR PAYMENT
            // if ($status != null && $status != $payment->payment_status) {
            //     $this->paymentRepo->update($payment->id, ["payment_status" => $status]);
            if ($payment->payment_status == "completed") {
                $this->paymentRepo->assignEmails($payment->id, $payment->id_email_type, $payment->quantity);
                $this->paymentRepo->update($payment->id, ['is_assigned_emails' => 1]);
                
                $payment->sendMailUrlTxt = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'txt']);
                $payment->sendMailUrlXlsx = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'xlsx']);

                return response()->json([
                    'payment'=>$payment
                ]);
            } 
            return response()->json([
                'message'=>'Please pay for this invoice!!'
            ],500);
            // }

            
        } else if ($type == Config::get("app_private.invoice_type.mua_hang_tra_truoc")) {
            //GET PAYMENT IN DATABASE
            $payment = $this->paymentRepo->getPayment($orderId);
               
            if(empty($payment)) {
                return response()->json([
                    'message'=>'Nothing found!!'
                ],500);
            }
            if ($payment->is_assigned_emails == 0) {
                $this->paymentRepo->assignEmails($payment->id, $payment->id_email_type, $payment->quantity);
                $this->paymentRepo->update($payment->id, ['is_assigned_emails' => 1]);
            }
            $payment->sendMailUrlTxt = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'txt']);
            $payment->sendMailUrlXlsx = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'xlsx']);

            return response()->json([
                'payment'=>$payment
            ]);
        } else if ($type == Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan")) {
            //GET PAYMENT IN DATABASE
            $payment = $this->paymentRepo->getPayment($orderId);
               
            if(empty($payment)) {
                return response()->json([
                    'message'=>'Nothing found!!'
                ],500);
            }

            //GET PAYMENT REAL STATUS
            // $status = null;
            // if ($payment->payment_status != "finished") {
            //     $status = $this->paymentRepo->getStatusInLive($payment->payment_id);
            // } else {
            //     $status = "finished";
            // }
            
            //UPDATE PAYMENT IF CHANGED AND ASSIGN EMAILS FOR PAYMENT
            // if ($status != null && $status != $payment->payment_status) {
            //     $this->paymentRepo->update($payment->id, ["payment_status" => $status]);
            if ($payment->payment_status == "completed") {
                $userMoney = $this->userMoneyModel->where([
                    "currency_code" => Config::get("app_private.currency.usd"),
                    "user_id" => $payment->id_customer,
                ])->first();
                if(empty($userMoney)) {
                    $this->userMoneyModel->create([
                        "currency_code" => Config::get("app_private.currency.usd"),
                        "total_amount" => $payment->price_amount,
                        "user_id" => $payment->id_customer,
                    ]);
                } else {
                    $newAmount = $userMoney->total_amount + $payment->price_amount;
                    $userMoney->update(["total_amount" => $newAmount]);
                }
                
            }
            // }
            $payment->sendMailUrlTxt = "";
            $payment->sendMailUrlXlsx = "";

            return response()->json([
                'payment'=>$payment
            ]);
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function callback_fpayment(Request $request)
    {
        $searchQuery = $request->query->all();
        if(empty($searchQuery['request_id'])){
            return response()->json([
                'message'=>'request_id empty'
            ],500);
        }
        if(empty($searchQuery['token'])){
            return response()->json([
                'message'=>'token empty'
            ],500);
        }
        if(empty($searchQuery['status'])){
            return response()->json([
                'message'=>'status empty'
            ],500);
        }
        $this->paymentRepo->accessCallBack($searchQuery);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function getPaymentByInvoiceId($invoiceId, $userId)
    {
        //GET PAYMENT IN DATABASE
        $payment = $this->paymentRepo->getPaymentByInvoiceId($invoiceId, $userId);
        
        if(empty($payment)) {
            return response()->json([
                'message'=>'Nothing found!!'
            ],500);
        }

        //GET PAYMENT REAL STATUS
        $status = $this->paymentRepo->getStatusInLive($payment->payment_id);

        //UPDATE PAYMENT IF CHANGED AND ASSIGN EMAILS FOR PAYMENT
        if ($status != null && $status != $payment->payment_status) {
            $this->paymentRepo->update($payment->id, ["payment_status" => $status]);
            if ($status == "finished") {
                $this->paymentRepo->assignEmails($payment->id, $payment->id_email_type, $payment->quantity);
            }
        }

        $payment->sendMailUrlTxt = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'txt']);
        $payment->sendMailUrlXlsx = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $payment->id_customer . "|" . $payment->id) , 'type' => 'xlsx']);

        return response()->json([
            'payment'=>$payment
        ]);
    }

    
    public function search($payment_id)
    {
        //GET PAYMENT IN DATABASE
        $payment = $this->paymentRepo->find($payment_id);
        //GET Full PAYMENT IN DATABASE
        $invoice = $this->invoiceRepo->find($payment->id_invoice);
        
        if(empty($payment)) {
            return response()->json([
                'message'=>'Nothing found!!'
            ],500);
        }
        
        $payment->sendMailUrlTxt = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $invoice->id_customer . "|" . $payment->id) , 'type' => 'txt']);
        $payment->sendMailUrlXlsx = URL::route('getEmailsPayment', ['hashStr' => $this->helperClass->enCryptDeCryptMethod ('encrypt', $invoice->id_customer . "|" . $payment->id) , 'type' => 'xlsx']);

        return response()->json([
            'payment'=>$payment
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}