<?php

namespace App\Http\Controllers;

use App\Models\Email;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importEmail(Request $request)
    {
        $request->validate([
            'file'=>'required|max:10000',
        ]);

        $file = $request->file('file');
        $data = $data = $request->all();
        if ($file) {
            $filename = $file->getClientOriginalName();
            //Where uploaded file will be stored on the server 
            $location = 'uploads'; //Created an "uploads" folder for that
            // Upload file
            $file->move($location, $filename);
            // In case the uploaded file path is to be stored in the database 
            $filepath = public_path($location . "/" . $filename);
            // Reading file
            $file = fopen($filepath, "r");
            $importData_arr = array(); // Read through the file and store the contents as an array
            $i = 0;
            //Read the contents of the uploaded file 
            while (($filedata = fgetcsv($file, 10000, "|")) !== FALSE) 
            {
                $num = count($filedata);
                // // Skip first row (Remove below comment if you want to skip the first row)
                // if ($i == 0) {
                //     $i++;
                //     continue;
                // }
                for ($c = 0; $c < $num; $c++) {
                    $importData_arr[$i][] = $filedata[$c];
                }
                $i++;
            }
            
            fclose($file); //Close after reading
            $j = 0;
            try 
            {
                DB::beginTransaction();
                foreach ($importData_arr as $importData) {
                    $j++;
                    Email::create([
                        'email' => $importData[0],
                        'password' => $importData[1],
                        'recovery_email' => $importData[2],
                        'id_email_type' => (int)$data['type'],
                        'ip' => $importData[3],
                        'date_created' => $importData[4],
                        'status' => 1,
                    ]);
                }
                DB::commit();
                return response()->json([
                    'message' => "$j records successfully uploaded"
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'message' => $e
                    ]);
                DB::rollBack();
            }
        } else {
            //no file was uploaded
            throw new \Exception('No file was uploaded', Response::HTTP_BAD_REQUEST);
        }
    }
}
