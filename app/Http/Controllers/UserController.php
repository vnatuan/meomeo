<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Str;

class UserController extends Controller
{
     /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $UserRepo;
    protected $helperClass;

    public function __construct(
        UserRepositoryInterface $UserRepo
    )
    {
        $this->UserRepo = $UserRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::select('id','name', 'email', 'password', 'role','api_token', 'status')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
            'role' => 'role'
        ]);

        $requestData = $request->all();


        try{
            $insetData = [
                'name' => $requestData['name'],
                'email' => $requestData['email'],
                'role' => $requestData['role'],
                'password' => Hash::make($requestData['password']),
                'api_token' => Str::random(60),
            ];
            
            User::create($insetData);

            return response()->json([
                'message'=>'User Created Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while creating a User!!'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show(User $User)
    {
        return response()->json([
            'User'=>$User
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $User)
    {
        $request->validate([
            'User'=>'required',
            'recovery_User'=>'required'
        ]);

        $requestData = $request->all();

        try{
            $requestData['password'] = Hash::make($requestData['password']);

            $User->fill($requestData)->update();

            return response()->json([
                'message'=>'User Updated Successfully!!'
            ]);

        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while updating a User!!'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $User)
    {
        try {
            $User->delete();

            return response()->json([
                'message'=>'User Deleted Successfully!!'
            ]);   
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while deleting a User!!'
            ]);
        }
    }
}
