<?php

namespace App\Http\Controllers;

use App\Helper\CryptoHelper;
use Illuminate\Http\Request;

class CryptoController extends Controller
{
    public $cryptoHelper;

    public function __construct(CryptoHelper $cryptoHelper)
    {
        $this->cryptoHelper = $cryptoHelper;
    }

    public function nowpaymentCreateInvoice(Request $request)
    {
        $request->validate([
            'price_amount'=>'required',
            'price_currency' => 'required',
            'pay_currency'=>'required',
            'order_id' => 'required',
            'order_description' => 'required',
        ]);

        try{  
            $requestData = $request->all();
            $result = $this->cryptoHelper->nowpayemntCreateInvoice_sandBox($requestData['price_amount'], $requestData['price_currency'], 
                                    $requestData['pay_currency'], $requestData['order_id'], $requestData['order_description']);
            return response()->json([
                'status' => $result
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message' => 'Something goes wrong while creating a Customer!!'
            ],500);
        }
    }
}
