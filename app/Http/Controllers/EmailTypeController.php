<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EmailType\EmailTypeRepositoryInterface;


class EmailTypeController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $emailTypeRepo;

    public function __construct(EmailTypeRepositoryInterface $emailTypeRepo)
    {
        $this->emailTypeRepo = $emailTypeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailTypes = $this->emailTypeRepo->getAll();
        return $emailTypes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmailType  $EmailType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $emailType = $this->emailTypeRepo->find($id);

        return response()->json([
            $emailType
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmailType  $EmailType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailType $emailType)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmailType  $EmailType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmailType $emailType)
    {
        
    }
}