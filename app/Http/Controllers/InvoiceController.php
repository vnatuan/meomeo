<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests\PostInvoiceRequest;
use App\Repositories\Invoice\InvoiceRepositoryInterface;
use App\Repositories\Customer\CustomerRepositoryInterface;
use App\Repositories\EmailType\EmailTypeRepositoryInterface;
use App\Models\User as UserModel;
use App\Repositories\UserMoney\UserMoneyRepositoryInterface;
use App\Helper\Helper;
use Illuminate\Support\Facades\Config;

class InvoiceController extends Controller
{
    /**
     * @var PostRepositoryInterface|\App\Repositories\Repository
     */
    protected $invoiceRepo;
    protected $customerRepo;
    protected $emailTypeRepo;
    protected $userMoneyRepo;
    protected $userModel;
    public $helperClass;
    public $nowpaymentSuccessUrl;

    public function __construct(
        InvoiceRepositoryInterface $invoiceRepo,
        CustomerRepositoryInterface $customerRepo,
        EmailTypeRepositoryInterface $emailTypeRepo,
        UserMoneyRepositoryInterface $userMoneyRepo,
        UserModel $userModel,
        Helper $helperClass
    ) {
        $this->middleware('auth:api', ['except' => ['index', 'store', 'show', 'update', 'detail', 'getHoaDonByUser']]);
        $this->invoiceRepo = $invoiceRepo;
        $this->customerRepo = $customerRepo;
        $this->emailTypeRepo = $emailTypeRepo;
        $this->userMoneyRepo = $userMoneyRepo;
        $this->userModel = $userModel;
        //Define Class
        $this->helperClass = $helperClass;
        $this->nowpaymentSuccessUrl = env('NOWPAYMENT_SUCCESS_URL');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = $this->invoiceRepo->getAll();
        return $invoices;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostInvoiceRequest $request)
    {
        $data = $request->all();
        
        $invoice = null;
        try {
            //Check quantity
            $inStock = $this->emailTypeRepo->checkInStock($data['id_email_type'], $data['quantity']);
            
            if (!$inStock) {
                return response()->json([
                    'message'=> "This email type don't have enoungh quantity!"
                ], 500);
            }
            if ($data['is_user'] != "1") {
                if (floatval($data['quantity']) < 50) {
                    return response()->json([
                        'message'=> "Min quantity is 50 !"
                    ], 500);
                }
                //For Customer
                //1 - check
                $customer = $this->customerRepo->where(['email' => $data['customer_email']])->first();
                //2 - register if don't exist
                if (empty($customer)) {
                    $customer = $this->customerRepo->create(['email' => $data['customer_email']]);
                }
                
                $data['id_customer'] = $customer->id;
                //Email Type
                $emailType = $this->emailTypeRepo->where(['id' => $data['id_email_type']])->first();
                $data['price'] = $emailType->price;
                $data['is_user'] = $data['is_user'];
                $data['type'] = Config::get("app_private.invoice_type.mua_hang_tra_sau");
                // $data['role'] = ($data['is_user'] = "1") ? $customer->role : null;
                //For Invoice
                $invoice = $this->invoiceRepo->createInvoiceTraSau_Fpayment($data);
            }
            return response()->json([
                $invoice
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message'=> $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = $this->invoiceRepo->find($id);

        return response()->json([
            'invoice'=>$invoice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'description'=>'required'
        ]);

        $data = $request->all();

        try {
            $this->invoiceRepo->update($id, $data);

            return response()->json([
                'message'=>'Invoice Updated Successfully!!'
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message'=>'Something goes wrong while updating a Invoice!!'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $Invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->invoiceRepo->delete($id);

            return response()->json([
                'message'=>'Invoice Deleted Successfully!!'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message'=>'Something goes wrong while deleting a Invoice!!'
            ]);
        }
    }

    public function getHoaDonByUser($user_id, $type)
    {
        if ($type != Config::get("app_private.invoice_type.mua_hang_tra_sau") 
            && $type != Config::get("app_private.invoice_type.mua_hang_tra_truoc") 
            && $type != Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan") ) {
                return response()->json([
                    'message'=>'Type not found!!'
                ], 500);
            }
        $donhangs = $this->invoiceRepo->layDanhSachHoaDonByUserId($user_id, $type);
       
        foreach ($donhangs as $key=>$donhang) {
            $encryptStr = $this->helperClass->enCryptDeCryptMethod('encrypt', $donhang->order_id . "|||" . $type);
            $donhangs[$key]->link = $this->nowpaymentSuccessUrl . $encryptStr;
            $donhangs[$key]->date_created =  date("d-m-Y",strtotime($donhang->created_at));
        }
        

        //GET PAYMENT IN DATABASE
        //$payment = $this->paymentRepo->find($payment_id);
        //GET Full PAYMENT IN DATABASE
        //$invoice = $this->invoiceRepo->find($payment->id_invoice);

        if (empty($donhangs)) {
            return response()->json([
                'message'=>'Nothing found!!'
            ], 500);
        }

        return response()->json([
            'donhang'=>$donhangs
        ]);
    }

    public function muaEmailTraTruoc(PostInvoiceRequest $request) {
        $data = $request->all();
        $invoice = null;
        try {
            //Check quantity
            $inStock = $this->emailTypeRepo->checkInStock($data['id_email_type'], $data['quantity']);
            if (!$inStock) {
                return response()->json([
                    'message'=> "This email type don't have enoungh quantity!"
                ], 500);
            }
            if ($data['is_user'] == "1") {
                //For USer
                //1 - check
                $user = $this->userModel->where(['email' => $data['customer_email']])->first();
                //2 - register if don't exist
                if (empty($user)) {
                    return response()->json([
                        'message'=> "User is not exist!"
                    ], 500);
                }
                //Email Type
                $emailType = $this->emailTypeRepo->where(['id' => $data['id_email_type']])->first();
                $data['price'] = $emailType->price;
                //Check Amount
                $amount = floatval($data['quantity']) * floatval($data['price']);
                
                $userAmount = $this->userMoneyRepo->where(['user_id' => $user->id, 'currency_code' => Config::get("app_private.currency.usd")])->first();
                
                if (empty($userAmount) || $userAmount->total_amount < $amount) {
                    return response()->json([
                        'message'=> "Your account don't have enough amount , please deposit money !"
                    ], 500);
                }
                $data['id_customer'] = $user->id;
                $data['is_user'] = $data['is_user'];
                $data['type'] = Config::get("app_private.invoice_type.mua_hang_tra_truoc");
                //For Invoice
                $invoice = $this->invoiceRepo->createInvoiceTraTruoc($data);
                $newAmount = $userAmount->total_amount - $amount;
                $userAmount->update(["total_amount" => $newAmount]);
            }
           
            return response()->json([
                $invoice
            ]);
        } catch(\Exception $e) {
            return response()->json([
                'message'=> $e->getMessage()
            ], 500);
        }
    }
}