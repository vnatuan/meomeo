<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth:api', ['except' => ['login', 'register', 'changePassWord']]);
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            $errorStr = '';
        
            foreach($validator->errors()->messages() as $key=>$errors) {
                foreach($errors as $error) {
                    $errorStr  .= $error.'\n';
                }
            }
            return response()->json(['message' => $errorStr], 422);
        }

        if (! $token = auth('api')->attempt($validator->validated())) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'role' => 'required|integer|in:1,2',
        ]);

        if($validator->fails()){
            $errorStr = '';
        
            foreach($validator->errors()->messages() as $key=>$errors) {
                foreach($errors as $error) {
                    $errorStr  .= $error.'\n';
                }
            }
            return response()->json(['message' => $errorStr], 400);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth('api')->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth('api')->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }

    public function changePassWord(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|string|min:6',
            'new_password' => 'required|string|confirmed|min:6',
        ]);
 
        if($validator->fails()){
            $errorStr = '';
        
            foreach($validator->errors()->messages() as $key=>$errors) {
                foreach($errors as $error) {
                    $errorStr  .= $error.'\n';
                }
            }
            return response()->json(['message' => $errorStr], 400);
        }
        $userId =auth('api')->user()->id;
 
        $user = User::where('id', $userId)->update(
                    ['password' => bcrypt($request->new_password)]
                );
 
        return response()->json([
            'message' => 'User successfully changed password',
            'user' => $user,
        ], 201);
    }

    public function forgotPassword(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:100|exists:users,email',
        ]);
 
        if($validator->fails()){
            $errorStr = '';
        
            foreach($validator->errors()->messages() as $key=>$errors) {
                foreach($errors as $error) {
                    $errorStr  .= $error.'\n';
                }
            }
            return response()->json(['message' => $errorStr], 400);
        }
        $data = $request->all();
        $newPassword = Str::random(10);
        $user = User::where('email', $data['email'])->update(
            ['password' => bcrypt($newPassword)]
        );
        Mail::send([], $data, function($message) use ($data, $newPassword){
            $message->to($data['email'])->subject('Meomeo.io new password');
            //$message->to('vnatuan1989@gmail.com')->subject('This is test email');
    
            $message->from('vnatuan1989@gmail.com', "Meomeo.io");
            $message->setBody('<h1>Hi,</h1><p>This is your new password : <b>'. $newPassword .'</b></p>', 'text/html'); 
    
        });
 
        return response()->json([
            'message' => 'User successfully changed password',
            'user' => $user,
        ], 201);
    }
}