<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Contracts\Validation\Validator;

class PostInvoiceAddAmountRequest extends FormRequest
{
    public function rules()
    {
        return [
            'user_email'=>'required|email',
            'amount'=>'required|numeric|min:100',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $errorStr = '';
        
        foreach($validator->errors()->messages() as $key=>$errors) {
            foreach($errors as $error) {
                $errorStr  .= $error.'\n';
            }
        }
        throw new HttpResponseException(response()->json([
            'message'   => $errorStr
        ],500));
    }

    public function messages()
    {
        return [
            'user_email.required' => 'Customer email is required',
            'customer_email.email' => 'Customer email is not valid',
            'amount.required' => 'Amount is required',
            'amount.min' => 'Min amount is 100 USDT',
            'amount.numeric' => 'Amount must be numeric'
        ];
    }
}