<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Contracts\Validation\Validator;

class PostInvoiceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'id_email_type'=>'required',
            'customer_email'=>'required|email',
            'quantity'=>'required|numeric',
            'payment_type'=>'required',
            'is_user'=>'required|in:0,1'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $errorStr = '';
        
        foreach($validator->errors()->messages() as $key=>$errors) {
            foreach($errors as $error) {
                $errorStr  .= $error.'\n';
            }
        }
        throw new HttpResponseException(response()->json([
            'message'   => $errorStr
        ],500));
    }

    public function messages()
    {
        return [
            'id_email_type.required' => 'Email type is required',
            'customer_email.required' => 'Customer email is required',
            'customer_email.email' => 'Customer email is not valid',
            'quantity.required' => 'Quantity is required',
            'payment_type.required' => 'Payment type is required',
            'is_user.required' => 'Is user is required',
            'is_user.in' => 'Is user is 0 or 1'
        ];
    }
}