<?php
namespace App\Services;

use App\Repositories\Customer\CustomerRepository;
use App\Repositories\Email\EmailRepository;
use App\Repositories\Invoice\InvoiceRepository;
use App\Repositories\Payment\PaymentRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\UserMoney\UserMoneyRepository;

class AdminServices
{
    private $customerRepository;
    private $emailRepository;
    private $invoiceRepository;
    private $paymentRepository;
    private $productRepository;
    private $userMoneyRepository;

    public function __construct(
        CustomerRepository $customerRepository,
        EmailRepository $emailRepository,
        InvoiceRepository $invoiceRepository,
        PaymentRepository $paymentRepository,
        ProductRepository $productRepository,
        UserMoneyRepository $userMoneyRepository
        ) {
        $this->customerRepository = $customerRepository;
        $this->emailRepository = $emailRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->paymentRepository = $paymentRepository;
        $this->productRepository = $productRepository;
        $this->userMoneyRepository = $userMoneyRepository;
    }

    public function getCustomersInfo()
    {
        $customers = $this->customerRepository->getCustomersInfo();
        return $customers;
    }
    public function getCustomerInfo($email)
    {
        $customer = $this->customerRepository->getCustomerInfo($email);
        return $customer;
    }
    public function addMoneyCustomer($id,$amount)
    {
        $user_money = $this->userMoneyRepository->where(["user_id" => $id])->first();
        $user_money->total_amount = $user_money->total_amount + $amount;
        $user_money->save();
        return $user_money;
    }
}