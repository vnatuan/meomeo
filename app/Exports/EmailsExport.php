<?php
namespace App\Exports;

use App\Models\Email;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmailsExport implements WithMapping, FromCollection, WithHeadings
{
    private $emails;

    public function __construct($emails) {
        $this->emails = $emails;
    }

    public function collection()
    {
        return $this->emails;
    }

    public function headings(): array
    {
        return [
            'Email',
            'Password',
            'Recovery Email',
            'IP',
            'Date Created',
        ];
    }

    /**
    * Optional headers
    */
    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    public function map($email): array
    {
        return [
            $email->email,
            $email->password,
            $email->recovery_email,
            $email->ip,
            $email->date_created,
        ];  
    }
}
