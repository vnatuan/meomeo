<?php
namespace App\Helper;
use App\Helper\Helper;
use Exception;
use Illuminate\Support\Facades\Config;

class CryptoHelper
{
    public $helperClass;
    public $nowpaymentSandboxUsername;
    public $nowpaymentSandboxPassword;
    public $sandboxNowpaymentAPIKey;
    public $sandboxNowpaymentAPIUrl;
    public $sandboxNowpaymentAPIUrlWithAuth;

    public $nowpaymentUsername;
    public $nowpaymentPassword;
    public $nowpaymentAPIKey;
    public $nowpaymentAPIUrl;
    public $nowpaymentAPIUrlWithAuth;

    public $nowpaymentCbUrl;
    public $nowpaymentSuccessUrlTraSau;
    public $nowpaymentSuccessUrlAddAmount;
    public $nowpaymentCancelUrl;
    public function __construct(Helper $helperClass)
    {
        //Define Class
        $this->helperClass = $helperClass;
        //NOWPAYMENT
        $this->nowpaymentUsername = env('NOWPAYMENT_USERNAME');
        $this->nowpaymentPassword = env('NOWPAYMENT_PASSWORD');
        $this->nowpaymentAPIKey = env('NOWPAYMENT_API_KEY');
        $this->nowpaymentAPIUrl = env('NOWPAYMENT_API_URL');
        $this->nowpaymentAPIUrlWithAuth = env('NOWPAYMENT_API_URL_WITH_AUTH');
        //SANDBOX NOWPAYMENT
        $this->nowpaymentSandboxUsername = env('SANDBOX_NOWPAYMENT_USERNAME');
        $this->nowpaymentSandboxPassword = env('SANDBOX_NOWPAYMENT_PASSWORD');
        $this->sandboxNowpaymentAPIKey = env('SANDBOX_NOWPAYMENT_API_KEY');
        $this->sandboxNowpaymentAPIUrl = env('SANDBOX_NOWPAYMENT_API_URL');
        $this->sandboxNowpaymentAPIUrlWithAuth = env('SANDBOX_NOWPAYMENT_API_URL_WITH_AUTH');

        $this->nowpaymentCbUrl = env('NOWPAYMENT_CALLBACK_URL');
        $this->nowpaymentSuccessUrlTraSau = env('NOWPAYMENT_SUCCESS_URL_TRA_SAU');
        $this->nowpaymentSuccessUrlAddAmount = env('NOWPAYMENT_SUCCESS_URL_ADD_AMOUNT');
        $this->nowpaymentCancelUrl = env('NOWPAYMENT_CANCEL_URL');
    }

    public function nowpaymentCreateInvoice($amount, $currency, $payCurrency, $orderId, $orderDescription)
    {
        # Define function endpoint
        $ch = curl_init($this->nowpaymentAPIUrl . "invoice");

        # Setup request to send json via POST. This is where all parameters should be entered.
        $payload = json_encode( array("price_amount" => $amount, "price_currency" => $currency, "pay_currency" => $payCurrency, "order_id" => $orderId, "order_description" => $orderDescription) );

        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "x-api-key: " . $this->nowpaymentAPIKey));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        # Send request.
        $result = curl_exec($ch);
        
        curl_close($ch);

        # Decode the received JSON string
        $resultdecoded = json_decode($result, true);
        return $resultdecoded;
    }

    public function nowpayemntCreatePaymentForInvoice($invoiceId, $payCurrency, $customerEmail)
    {
        # Define function endpoint
        $ch = curl_init($this->nowpaymentAPIUrl . "invoice-payment");

        # Setup request to send json via POST. This is where all parameters should be entered.
        $payload = json_encode( array("iid" => $invoiceId, "pay_currency" => $payCurrency, "customer_email" => $customerEmail) );

        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "x-api-key: " . $this->nowpaymentAPIKey));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        # Send request.
        $result = curl_exec($ch);

        curl_close($ch);

        # Decode the received JSON string
        $resultdecoded = json_decode($result, true);
        return $resultdecoded;
    }
    //////////////////////SAND BOX//////////////////////////////

    public function nowpayemntCreateInvoiceSandBox($amount, $currency, $payCurrency, $orderId, $orderDescription, $type = "tratientructiep")
    {
        # Define function endpoint
        $successUrl = "/";
        try {
            $ch = curl_init($this->sandboxNowpaymentAPIUrl . "invoice");

            // Check if initialization had gone wrong*
            if ($ch === false) {
                throw new Exception('failed to initialize');
            }

            $encryptStr = $this->helperClass->enCryptDeCryptMethod('encrypt', $orderId . "|||" . $type);
            if ($type == Config::get("app_private.invoice_type.mua_hang_tra_sau")) {
                $successUrl = $this->nowpaymentSuccessUrlTraSau;
            }

            if ($type == Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan")) {
                $successUrl = $this->nowpaymentSuccessUrlAddAmount;
            }
            # Setup request to send json via POST. This is where all parameters should be entered.
            $payload = json_encode(
                array(
                    "price_amount" => $amount,
                    "price_currency" => $currency,
                    "pay_currency" => $payCurrency,
                    "order_id" => $orderId,
                    "order_description" => $orderDescription,
                    "ipn_callback_url" => $this->nowpaymentCbUrl . $encryptStr,
                    "success_url" => $successUrl . $encryptStr,
                    "cancel_url" => $this->nowpaymentCancelUrl . $encryptStr,
                )
            );

            
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "x-api-key: " . $this->sandboxNowpaymentAPIKey));

            # Return response instead of printing.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            # Send request.

            $result = curl_exec($ch);

            if ($result === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            curl_close($ch);

            # Decode the received JSON string
            $resultdecoded = json_decode($result, true);
            return $resultdecoded;
        } catch(Exception $e) {
            trigger_error(
                sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(),
                $e->getMessage()
            ),
                E_USER_ERROR
            );
        }
    }

    public function nowpayemntCreatePaymentForInvoiceSandBox($invoiceId, $payCurrency, $customerEmail)
    {
        # Define function endpoint
        $ch = curl_init($this->sandboxNowpaymentAPIUrl . "invoice-payment");

        # Setup request to send json via POST. This is where all parameters should be entered.
        $payload = json_encode( array("iid" => $invoiceId, "pay_currency" => $payCurrency, "customer_email" => $customerEmail) );

        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "x-api-key: " . $this->sandboxNowpaymentAPIKey));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        # Send request.
        $result = curl_exec($ch);

        curl_close($ch);

        # Decode the received JSON string
        $resultdecoded = json_decode($result, true);
        return $resultdecoded;
    }
    
    public function nowpayemntCheckPaymentStatusSandBox($paymentId)
    {
        # Define function endpoint
        $ch = curl_init($this->sandboxNowpaymentAPIUrl . "payment/$paymentId");

        # Setup request to send json via get. This is where all parameters should be entered.
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type:application/json", "x-api-key: " . $this->sandboxNowpaymentAPIKey));

        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        # Send request.
        $result = curl_exec($ch);

        curl_close($ch);

        # Decode the received JSON string
        $resultdecoded = json_decode($result, true);
        
        return $resultdecoded;
    }

    public function fpaymentCreateInvoice(
        $name,
        $description,
        $amount,
        $orderId,
        $type
    ) {

        $encryptStr = $this->helperClass->enCryptDeCryptMethod('encrypt', $orderId . "|||" . $type);
        if ($type == Config::get("app_private.invoice_type.mua_hang_tra_sau")) {
            $successUrl = $this->nowpaymentSuccessUrlTraSau;
        }

        if ($type == Config::get("app_private.invoice_type.nap_tien_vao_tai_khoan")) {
            $successUrl = $this->nowpaymentSuccessUrlAddAmount;
        }
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        $address_wallet = env('FPAYMENT_WALLET');
        $token_wallet = env('FPAYMENT_TOKEN');
        $callback_url = env('FPAYMENT_CALLBACK_URL');
        $return_url = $successUrl . $encryptStr;

        $result = file_get_contents('https://fpayment.co/api/AddInvoice.php?token_wallet='.$token_wallet.
            '&address_wallet='.$address_wallet.
            '&name='.urlencode($name).
            '&description='.urlencode($description).
            '&amount='.$amount.
            '&request_id='.$orderId.
            '&callback='.urlencode($callback_url).
            '&return_url='.urlencode($return_url), false, stream_context_create($arrContextOptions)
        );
        
        $result = json_decode($result, true);
        if(!isset($result['status'])){
            die(json_encode(['status' => 'error', 'msg' => __('Invoice could not be generated due to API error, please try again later')]));
        }
        if($result['status'] == 'error'){
            die(json_encode(['status' => 'error', 'msg' => __($result['msg'])]));
        }
        return $result;
    }
}
