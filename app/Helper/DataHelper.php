<?php
namespace App\Helper;
use Illuminate\Support\Facades\DB;

class DataHelper
{
    public function getProductEmails(int $productId)
   {
      $emails = DB::table("emails")
      ->join("email_types", "emails.id_email_type", "=", "email_types.id")
      ->join("product_email_type", "product_email_type.id_email_type", "=", "email_types.id")
      ->join("products", "product_email_type.id_product", "=", "products.id")
      ->select("emails.*")
      ->where("emails.status" , "=", 1)
      ->where("products.id" , "=", $productId)
      ->get()->toArray();
      return $emails;
   }
}
