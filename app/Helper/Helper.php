<?php
namespace App\Helper;
class Helper
{
    public $secretKey;
    public $secretIV;
    public $encryptMethod;
    public function __construct()
    {
        $this->secretKey = env('SECRET_KEY');
        $this->secretIV = env('SECRET_IV');
        $this->encryptMethod = env('ENCRYPT_METHOD');
    }

    public function enCryptDeCryptMethod($action, $string) 
    {
        $output = false;
        $encrypt_method = $this->encryptMethod;
        $secret_key = $this->secretKey;
        $secret_iv = $this->secretIV;
        // hash
        $key = hash('sha256', $secret_key);
    
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}
