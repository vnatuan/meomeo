<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMoney extends Model
{
    use HasFactory;

    protected $table = 'user_money';

    protected $fillable = ['user_id', 'currency_code', 'total_amount'];
}
