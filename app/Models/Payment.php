<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    
    protected $table = 'payments';

    protected $fillable = [
        "payment_id",
        "payment_status",
        "pay_address",
        "price_amount",
        "price_currency",
        "pay_amount",
        "amount_received",
        "pay_currency",
        "purchase_id",
        "id_invoice",
        "is_assigned_emails"
    ];
}
