<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailType extends Model {

   use HasFactory;

   protected $table = 'email_types';

   protected $fillable = ['description', 'ip_create', 'date_create','price'];

   public function toArray()
   {
      $array = parent::toArray();
      foreach ($this->getMutatedAttributes() as $key)
      {
         if ( ! array_key_exists($key, $array)) {
               $array[$key] = $this->{$key};   
         }
      }
      return $array;
   }

   public function emails()
   {
      return $this->hasMany(Email::class, 'id_email_type');
   }

   public function getInStockAttribute()
   {
      $count = DB::table("emails")
         ->join("email_types", "emails.id_email_type", "=", "email_types.id")
         ->select("emails.*")
         ->where("emails.status" , "=", 1)
         ->where("email_types.id" , '=', $this->id)
         ->get()->count();
      return $count;
   }

   // public function products() 
   // {
   //    return $this->belongsToMany(Product::class);
   // }
}

?>