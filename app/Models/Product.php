<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model {

    use HasFactory;

    protected $table = 'products';

    protected $fillable = ['title', 'description'];

    public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

    public function getInStockAttribute()
    {
        $count = DB::table("emails")
            ->join("email_types", "emails.id_email_type", "=", "email_types.id")
            ->join("product_email_type", "product_email_type.id_email_type", "=", "email_types.id")
            ->join("products", "product_email_type.id_product", "=", "products.id")
            ->select("emails.*")
            ->where("emails.status" , "=", 1)
            ->where("products.id" , '=', $this->id)
            ->get()->count();
        return $count;
    }

    public function getMinPriceAttribute()
    {
        $item = DB::table("email_types")
            ->join("product_email_type", "product_email_type.id_email_type", "=", "email_types.id")
            ->join("products", "product_email_type.id_product", "=", "products.id")
            ->selectRaw("MIN(price) as min_price")
            ->where("products.id" , '=', $this->id)
            ->first();
        return $item->min_price;
    }

    public function getEmailTypes()
    {
        $emailTypes = DB::table("email_types")
            ->join("product_email_type", "product_email_type.id_email_type", "=", "email_types.id")
            ->join("products", "product_email_type.id_product", "=", "products.id")
            ->selectRaw("email_types.*")
            ->where("products.id" , '=', $this->id)
            ->get()->toArray();
        return $emailTypes;
    }

    public function emailTypes() 
    {
        return $this->belongsToMany(EmailType::class, 'product_email_type', 'id_product', 'id_email_type');
    }
}

?>