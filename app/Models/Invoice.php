<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoices';

    protected $fillable = ['invoice_url', 'pay_currency','price_currency','price_amount','order_description','iid',
    'order_id','token_id','id_customer','id_product','id_email_type','payment_type','quantity',"is_user","type"];
}
